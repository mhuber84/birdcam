# Birdcam

[Raspi Zero 2 W](https://www.berrybase.de/raspberry-pi-zero-2-w) mit [NoIR Kamera](https://www.berrybase.de/noir-kamera-fuer-raspberry-pi-mit-fisheye-lens-einstellbarem-fokus-und-infrarot-leds) 
und USB-Micro in einem selbst gebastelten Nistkasten aus Holz.

Solar hat leider nicht funktioniert, selbst mit zwischengeschaltener Powerbank hat das nicht geklappt. Auch mit Zusatzunterstützung 
durch ein Netzteil hats nicht stabil funktioniert. Vermutlich liegt das aber eher an dem 5m-USB-Kabel vom Solar zum Raspi ;-)

Auf dem Raspi (debian bookworm desktop 32bit) läuft ein Python-Skript, siehe [/birdcam](/birdcam) und [Doku](https://datasheets.raspberrypi.com/camera/picamera2-manual.pdf) 
und [GitHub](https://github.com/raspberrypi/picamera2). Aktuell läuft [birdcam/stream_ffmpeg.py](birdcam/stream_ffmpeg.py), das macht nur Streaming 
ohne Capturing mit Motion Detection. Aber mit Ton. Wobei da vermutlich auch nur ffmpeg ohne Python reichen würde, wenn ich 
Motion-Capture auf einen anderen Server auslagere. Der Zero 1 hat Stream und Capture geschafft, aber nur ohne Ton. Ton hat 
da gar nicht funktioniert. Der 2er schafft leider auch nicht alles gleichzeitig. Aber die Bildqualität ist deutlich besser 
und Ton funktioniert stabil im Stream. Man merkt die 4 CPU-Kerne beim Zero 2 statt nur 1 Kern beim Zero 1.

Ein Versuch ganz ohne Python findet sich in [birdcam/stream_ffmpeg.sh](birdcam/stream_ffmpeg.sh). Für den Text im Video musste ich aber rpicam-vid selbst kompilieren.

Das startet per systemd automatisch: [birdcam/birdcam.service](birdcam/birdcam.service)
```
systemctl --user enable /home/pi/birdcam/birdcam.service
systemctl --user start birdcam.service
systemctl --user stop birdcam.service
systemctl --user status birdcam.service
journalctl --user -xeu birdcam.service
```

Auf einem Server läuft [MediaMTX](https://github.com/bluenviron/mediamtx) im Docker-Container: [/mediamtx](/mediamtx). Anfangs, 
weil ich mit rtsp-Streams experimentiert habe. Jetzt läuft er noch, weil ich so nur einen Stream vom schwachen Raspi zum
starken Server habe und von dort mehrere Streams zu den Zuschauenden. Und MediaMTX kann viele Ausgabeformate: rtsp, hls, WebRTC.... Da soll auch noch die Aufzeichnung mit Motion Caputure
rein, da da bin ich aber noch ganz am Anfang. Python opencv ist einfach, da verliere ich aber den Ton. Mal schaun...

Am Anfang hab ich zur Aufnahme den Video-Stream im Browser per Screenrecording aufgenommen. Das freihand gezogene Aufnahmefenster hat Maße gehabt, die nicht alle Video-Player abspielen konnten. [Bildschirmaufzeichnungen/resize.sh](Bildschirmaufzeichnungen/resize.sh) behebt das.

Ansehen kann ich den Stream von MediaMTX mit http://mediamtx.sol:8888/birdcam/ im Browser. Aufzeichnung starte ich von Hand:
```
ffmpeg -rtsp_transport tcp -i rtsp://mediamtx.sol:8554/birdcam -c copy "Birdcam-$(date +"%Y-%m-%d-%H-%M-%S").mp4"
```
Einzelne Bilder gehn auch:
```
ffmpeg -rtsp_transport tcp -i rtsp://mediamtx.sol:8554/birdcam -c copy "Birdcam-$(date +"%Y-%m-%d-%H-%M-%S").jpg"
```

Videos gibts auf [@mhuber84@troet.cafe](https://troet.cafe/@mhuber84) mit dem Hashtag [#birdcam](https://troet.cafe/tags/birdcam).

![Nistkasten aus Holz, Außenansicht, am Einflugloch sitzt eine Kohlmeise](IMG_20240502_110759.jpg "Nistkasten aus Holz, Außenansicht, am Einflugloch sitzt eine Kohlmeise")

![Nistkasten aus Holz, Deckel ist geöffnet. Man sieht das Fach mit dem Raspi Zero 2 und dem Kameramodul.](IMG_20240517_152616.jpg "Nistkasten aus Holz, Deckel ist geöffnet. Man sieht das Fach mit dem Raspi Zero 2 und dem Kameramodul.")

![Im Nistkasten von oben fotografiert. Aufnahme mit der Raspi-Kamera. Im Nest sitzen mehrere Kohlmeisen-Küken und recken die weit geöffneten Schnäbel nach oben. Die Körper sind im dunklen Nest kaum zu erkennen, nur die hellen, kreisrund geöffneten Schnäbel von 5 Küken sind gut zu sehen.](Birdcam-2024-05-07-03-25-31.jpg "Im Nistkasten von oben fotografiert. Im Nest sitzen mehrere Kohlmeisen-Küken und recken die weit geöffneten Schnäbel nach oben. Die Körper sind im dunklen Nest kaum zu erkennen, nur die hellen, kreisrund geöffneten Schnäbel von 5 Küken sind gut zu sehen.")

![Im Nistkasten von oben fotografiert. Aufnahme mit der Handy-Kamera, nicht mit dem Raspi. Im Nest sitzen mehrere Kohlmeisen-Küken.](IMG_20240517_153407.jpg "Im Nistkasten von oben fotografiert. Aufnahme mit der Handy-Kamera, nicht mit dem Raspi. Im Nest sitzen mehrere Kohlmeisen-Küken")


Ja, alle Hosts in meinem Netzwerk haben Namen von Objekten aus dem Sonnensystem (sol, gaia, webb, euclid, jupiter, earth, 
...) ;-)

