#!/usr/bin/python3
from picamera2 import Picamera2, MappedArray
from picamera2.encoders import H264Encoder, Quality
from picamera2.outputs import FfmpegOutput
from libcamera import Transform
import os, time, cv2

# Resolution that we write h264 files to stream and record 
res = (1920, 1080)
#res = (1280, 720)

# Low resolution capture for motion detection
lsize = (320, 240)
#lsize = (640, 480)

# FPS
frame_rate = 30
micro = int(1000000 / frame_rate)

# Make sure to set correct tuning file for NOIR camera
tuning = Picamera2.load_tuning_file("imx219_noir.json")
picam2 = Picamera2(tuning=tuning)

video_config = picam2.create_video_configuration(
    main = {"size": res, "format": "RGB888"},
    lores = {"size": lsize, "format": "YUV420"},
    transform = Transform(vflip = True, hflip = False),
    controls = {"FrameRate": frame_rate, "FrameDurationLimits": (micro, micro)},
    buffer_count = 12
)
picam2.configure(video_config)

# text on video
def apply_timestamp(request):
    colour = (0, 255, 0)
    origin = (30, 30)
    font = cv2.FONT_HERSHEY_SIMPLEX
    scale = 1
    thickness = 2
    timestamp = time.strftime("%d.%m.%Y %X")
    textonvideo = "Birdcam " + str(timestamp)
    with MappedArray(request, "main") as m:
        cv2.putText(m.array, textonvideo, origin, font, scale, colour, thickness)
picam2.pre_callback = apply_timestamp

print("trying to start camera streams")
picam2.start()

def server():
    global picam2, res, lsize, frame_rate

    # print("trying to start streaming http://mediamtx.sol:8888/birdcam/")
    # print("trying to start streaming http://webb/birdcam/stream/ramdrive/stream.mpd")
    print("trying to start streaming http://webb/birdcam/stream/ramdrive/stream.m3u8")
    audio = True
    audio_device = "alsa_input.usb-C-Media_Electronics_Inc._USB_PnP_Sound_Device-00.mono-fallback"
    # see  https://thson.de/2020/12/19/bessere-audioqualitat-fuer-mikrofone-mit-pulseaudio-webrtc-und-rnnoise/
    # audio_device = "WebRTC_source"
    audio_sync = 0
    audio_samplerate = 48000
    audio_codec = "aac"
    #audio_bitrate = 128000
    audio_bitrate = 96000
    # outputtarget = "-f rtsp -rtsp_transport tcp rtsp://mediamtx.sol:8554/birdcam"
    # outputtarget = "-f dash -seg_duration 1 -streaming 1 -window_size " + str(frame_rate) + " -remove_at_exit 1 -use_template 1 -use_timeline 1 /home/pi/birdcam/stream/ramdrive/stream.mpd"
    outputtarget = "-f hls -hls_time 1 -hls_list_size " + str(frame_rate) + " -hls_flags delete_segments /home/pi/birdcam/stream/ramdrive/stream.m3u8"
    ServerOutput = FfmpegOutput(outputtarget, audio, audio_device, audio_sync, audio_samplerate, audio_codec, audio_bitrate)
    ServerEncoder = H264Encoder(repeat=True, iperiod=10, framerate=frame_rate, enable_sps_framerate=True)
    picam2.start_encoder(ServerEncoder, ServerOutput, quality=Quality.LOW)
    while True:
        print('streaming... ' + str(time.strftime("%d.%m.%Y %X")))
        time.sleep(10)

try:
    server()
except Exception as e:
    print("Exception: ", e)
finally:
    print("exiting picamera2 streamer")
    picam2.stop_encoder()
