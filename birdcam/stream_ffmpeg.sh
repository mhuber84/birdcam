#!/bin/bash

# für text auf dem video durch post-processing mit opencv muss rpicam-vid selbst gebaut werden.
# https://www.raspberrypi.com/documentation/computers/camera_software.html#building-libcamera-and-rpicam-apps
#sudo apt install -y libcamera-dev libjpeg-dev libtiff5-dev libpng-dev
#sudo apt install libavcodec-dev libavdevice-dev libavformat-dev libswresample-dev
#sudo apt install -y git
# opencv für post-processing annotate_cv stage
#sudo apt-get install opencv-data
#sudo apt-get install opencv-doc
#sudo apt-get install libopencv-dev
# https://www.raspberrypi.com/documentation/computers/camera_software.html#building-rpicam-apps
#sudo apt install -y cmake libboost-program-options-dev libdrm-dev libexif-dev
#sudo apt install -y meson ninja-build
#git clone https://github.com/raspberrypi/rpicam-apps.git
#cd rpicam-apps
#meson setup build -Denable_libav=true -Denable_drm=false -Denable_egl=false -Denable_qt=false -Denable_opencv=true -Denable_tflite=false -Dneon_flags=armv8-neon
#meson compile -C build -j 2
#sudo meson install -C build

# ton und stream intern mit libav
#/home/pi/rpicam-apps/build/apps/rpicam-vid \
#--timeout 0 --width 1920 --height 1080 --framerate 30 --nopreview 1 --vflip 1 --tuning-file "/usr/share/libcamera/ipa/rpi/vc4/imx219_noir.json" --inline 1 --codec "libav" \
#--libav-audio 1 --audio-codec "aac" --audio-source "pulse" --audio-device "alsa_input.usb-C-Media_Electronics_Inc._USB_PnP_Sound_Device-00.mono-fallback" --audio-channels 1 --audio-bitrate 96000 \
#--post-process-file "/home/pi/birdcam/annontate_cv.json" \
#--output "/home/pi/birdcam/stream/ramdrive/stream.m3u8"

# ton und stream macht ffmpeg
/home/pi/rpicam-apps/build/apps/rpicam-vid \
--timeout 0 --width 1920 --height 1080 --framerate 30 --nopreview --vflip --tuning-file "/usr/share/libcamera/ipa/rpi/vc4/imx219_noir.json" --inline 1 --codec "h264" \
--post-process-file "/home/pi/birdcam/annontate_cv.json" \
--output - | \
ffmpeg -loglevel "warning" -y \
-itsoffset 0 -f "pulse" -sample_rate 48000 -thread_queue_size 1024 -i "alsa_input.usb-C-Media_Electronics_Inc._USB_PnP_Sound_Device-00.mono-fallback" \
-use_wallclock_as_timestamps 1 -thread_queue_size 256 -i - \
-b:a 96000 -c:a "aac" -ac 2 \
-c:v copy \
-f hls -hls_time 1 -hls_list_size 50 -hls_flags delete_segments "/home/pi/birdcam/stream/ramdrive/stream.m3u8"