#!/usr/bin/python3
from picamera2 import Picamera2, MappedArray
from picamera2.encoders import H264Encoder, MJPEGEncoder, Quality
from picamera2.outputs import FfmpegOutput, FileOutput
from libcamera import Transform
import os, time, cv2, numpy, threading, shutil

import io
import logging
import socketserver
from http import server

# Resolution that we write h264 files to stream and record 
#res = (1920, 1080)
res = (1280, 720)

# Low resolution capture for motion detection
lsize = (320, 240)
#lsize = (640, 480)

# FPS
frame_rate = 30
micro = int(1000000 / frame_rate)

# Make sure to set correct tuning file for NOIR camera
tuning = Picamera2.load_tuning_file("imx219_noir.json")
picam2 = Picamera2(tuning=tuning)

video_config = picam2.create_video_configuration(
    main = {"size": res, "format": "RGB888"},
    lores = {"size": lsize, "format": "YUV420"},
    transform = Transform(vflip = True, hflip = False),
    controls = {"FrameRate": frame_rate, "FrameDurationLimits": (micro, micro)},
    buffer_count = 12
)
picam2.configure(video_config)

# text on video
def apply_timestamp(request):
    colour = (0, 255, 0)
    origin = (30, 30)
    font = cv2.FONT_HERSHEY_SIMPLEX
    scale = 1
    thickness = 2
    timestamp = time.strftime("%d.%m.%Y %X")
    textonvideo = "Birdcam " + str(timestamp)
    with MappedArray(request, "main") as m:
        cv2.putText(m.array, textonvideo, origin, font, scale, colour, thickness)
picam2.pre_callback = apply_timestamp

print("trying to start camera streams")
picam2.start()

output = None
class StreamingOutput(io.BufferedIOBase):
    def __init__(self):
        self.frame = None
        self.condition = threading.Condition()

    def write(self, buf):
        with self.condition:
            self.frame = buf
            self.condition.notify_all()


class StreamingHandler(server.BaseHTTPRequestHandler):
    def do_GET(self):
        if self.path == '/stream.mjpg':
            self.send_response(200)
            self.send_header('Age', 0)
            self.send_header('Cache-Control', 'no-cache, private')
            self.send_header('Pragma', 'no-cache')
            self.send_header('Content-Type', 'multipart/x-mixed-replace; boundary=FRAME')
            self.end_headers()
            try:
                while True:
                    with output.condition:
                        output.condition.wait()
                        frame = output.frame
                    self.wfile.write(b'--FRAME\r\n')
                    self.send_header('Content-Type', 'image/jpeg')
                    self.send_header('Content-Length', len(frame))
                    self.end_headers()
                    self.wfile.write(frame)
                    self.wfile.write(b'\r\n')
            except Exception as e:
                logging.warning(
                    'Removed streaming client %s: %s',
                    self.client_address, str(e))
        else:
            self.send_error(404)
            self.end_headers()


class StreamingServer(socketserver.ThreadingMixIn, server.HTTPServer):
    allow_reuse_address = True
    daemon_threads = True

def server():
    global picam2, res, lsize, frame_rate, output

    print("trying to start streaming http://webb:8000/stream.mjpg")
    output = StreamingOutput()
    ServerOutput = FileOutput(output)
    ServerEncoder = MJPEGEncoder()
    picam2.start_encoder(ServerEncoder, ServerOutput, quality=Quality.LOW)
    address = ('', 8000)
    ServerServer = StreamingServer(address, StreamingHandler)
    ServerServer.serve_forever()

def capture():
    global picam2, res, lsize, frame_rate

    mse_thres = 30
    w, h = lsize
    prev = None
    isencoding = False
    ltime = 0

    print("trying to start motion detection")
    CaptureEncoder = H264Encoder(repeat=True, iperiod=10, framerate=frame_rate, enable_sps_framerate=True)
    while True:
        cur = picam2.capture_buffer("lores")
        cur = cur[: w * h].reshape(h, w)
        if prev is not None:
            # Measure pixels differences between current and previous frame
            mse = numpy.square(numpy.subtract(cur, prev)).mean()
            if mse > mse_thres:
                if not isencoding:
                    print("New Motion with mse " + str(mse))
                    isencoding = True
                    mse_thres = 15
                    #epoch = int(time.time())
                    epoch = time.strftime("%Y-%m-%d-%H-%M-%S")
                    filename = "Birdcam-" + str(epoch) + "-" + str(mse)

                    print("trying to capture video " + str(filename))
                    audio = True
                    audio_device = "alsa_input.usb-C-Media_Electronics_Inc._USB_PnP_Sound_Device-00.mono-fallback"
                    # see  https://thson.de/2020/12/19/bessere-audioqualitat-fuer-mikrofone-mit-pulseaudio-webrtc-und-rnnoise/
                    # audio_device = "WebRTC_source"
                    audio_sync = 0
                    audio_samplerate = 48000
                    audio_codec = "aac"
                    #audio_bitrate = 128000
                    audio_bitrate = 96000
                    CaptureOutput = FfmpegOutput("/home/pi/birdcam/stream/ramdrive/" + str(filename) + "-tmp.mp4", audio, audio_device, audio_sync, audio_samplerate, audio_codec, audio_bitrate)
                    picam2.start_encoder(CaptureEncoder, CaptureOutput, quality=Quality.LOW)

                    print("trying to capture picture")
                    still = picam2.capture_request()
                    still.save("main", "/home/pi/birdcam/stream/sddrive/camera-tmp.jpg")
                    still.release()
                    os.rename("/home/pi/birdcam/stream/sddrive/camera-tmp.jpg", "/home/pi/birdcam/stream/sddrive/camera.jpg")
                ltime = time.time()
            else:
                duration = time.time() - ltime
                if isencoding and duration > 5.0:
                    print("Motion ended after " + str(duration) + " seconds")
                    CaptureOutput.stop()
                    picam2.stop_encoder(CaptureEncoder)
                    time.sleep(5)
                    shutil.move("/home/pi/birdcam/stream/ramdrive/" + str(filename) + "-tmp.mp4", "/home/pi/birdcam/stream/sddrive/" + str(filename) + "-tmp.mp4")
                    if duration < 15.0:
                        #os.remove("/home/pi/birdcam/stream/sddrive/" + str(filename) + "-tmp.mp4")
                        os.rename("/home/pi/birdcam/stream/sddrive/" + str(filename) + "-tmp.mp4", "/home/pi/birdcam/stream/sddrive/" + str(filename) + "-short.mp4")
                    else:
                        os.rename("/home/pi/birdcam/stream/sddrive/" + str(filename) + "-tmp.mp4", "/home/pi/birdcam/stream/sddrive/" + str(filename) + ".mp4")
                    isencoding = False
                    mse_thres = 30
        prev = cur

try:
    ServerThread = threading.Thread(target=server, daemon=True)
    ServerThread.start()

    capture()
    #CaptureThread = threading.Thread(target=capture, daemon=True)
    #CaptureThread.start()
except Exception as e:
    print("Exception: ", e)
finally:
    print("exiting picamera2 streamer")
    picam2.stop_encoder()
